//**Iteración #1: Interacción con el DOM** Dado el siguiente HTML: (copiado en html)

//1.1 Usa querySelector para mostrar por consola el botón con la clase .showme

// let showBtn = document.querySelector('.showme');
// console.log(showBtn);

// //1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado
// let caught = document.querySelector('#pillado');
// console.log(caught);

// //1.3 Usa querySelector para mostrar por consola todos los p
// let allParr = document.querySelectorAll('p')
// console.log(allParr);

// //1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon
// let pkmnTeam = document.querySelectorAll('.pokemon');
// console.log(pkmnTeam);

// //1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo data-function="testMe".
// let functionTest = document.querySelectorAll('[data-function="testMe"]');
// console.log(functionTest);

// //1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo data-function="testMe".

// let arrayTest = Array.from(functionTest);
// console.log(arrayTest[2]);


//2.1 Inserta dinamicamente en un html un div vacio con javascript.

let newDiv = document.createElement("div");
document.body.appendChild(newDiv);

//2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
let otherDiv = document.createElement("div");
let parr = document.createElement("p");
otherDiv.appendChild(parr);
document.body.appendChild(otherDiv);
//2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
let anotherDiv = document.createElement("div");
for (let i = 0; i < 6; i++) {
    let parr2 = document.createElement("p");
    anotherDiv.appendChild(parr2);
    }
    document.body.appendChild(anotherDiv);
//2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
let parr3 = document.createElement("p");
parr3.innerHTML = 'Soy dinámico!';
document.body.appendChild(parr3);

//2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
let h2$$ = document.querySelector("h2");
h2$$.innerHTML = 'Wubba Lubba dub dub';

//2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
let ul1 = document.createElement("ul");
for (let i = 0; i < apps.length; i++) {
    const element = apps[i];
    let li1 = document.createElement("li");
    li1.innerHTML = element;
    ul1.appendChild(li1);
}
document.body.appendChild(ul1);

//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
let toRemove = document.querySelectorAll('.fn-remove-me');
toRemove.forEach(element => {
    document.body.removeChild(element);
});

// console.log(toRemove);

//2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. Recuerda que no solo puedes insertar elementos con .appendChild.
const brother = document.querySelector('div.fn-insert-here');

let parr4 = document.createElement("p");

brother.parentNode.insertBefore(parr4, brother.nextSibling);
parr4.innerHTML = 'Voy en medio!';

// console.log(parr4);



// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here

const inside = document.querySelectorAll('div.fn-insert-here');


inside.forEach (div => {
    let parr5 = document.createElement("p");
    div.appendChild(parr5);
    parr5.innerHTML = 'Voy aquí!';
});

console.log(inside);



